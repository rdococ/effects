--[[
Example effects for rdococ's status effect mod.

fast doubles your speed.
slow halves it.

jump multiplies your jump height by 1.5.

heavy doubles your gravity.
light halves your gravity.
float is light on steroids, it divides your gravity by 5. it works even better when combined with light.

health instantly heals you 1.5 hearts.
regeneration heals you at a rate of 1 half-heart per second.

damage instantly damages you for 1.5 hearts.
poison damages you at a rate of half a heart per second, but it cannot kill you.

Effects are case insensitive, and are run through string.lower before being added to minetest.registered_effects.
]]
effects.register("fast", {
	on_apply = function (player, data)
		local override = player:get_physics_override()
		override.speed = override.speed * 2
		player:set_physics_override(override)
	end,
	on_join = function (player, data)
		local override = player:get_physics_override()
		override.speed = override.speed * 2
		player:set_physics_override(override)
	end,
	on_end = function (player, data)
		local override = player:get_physics_override()
		override.speed = override.speed / 2
		player:set_physics_override(override)
	end
})
effects.register("slow", {
	on_apply = function (player, data)
		local override = player:get_physics_override()
		override.speed = override.speed / 2
		player:set_physics_override(override)
	end,
	on_join = function (player, data)
		local override = player:get_physics_override()
		override.speed = override.speed / 2
		player:set_physics_override(override)
	end,
	on_end = function (player, data)
		local override = player:get_physics_override()
		override.speed = override.speed * 2
		player:set_physics_override(override)
	end
})

effects.register("jump", {
	on_apply = function (player, data)
		local override = player:get_physics_override()
		override.jump = override.jump * 1.5
		player:set_physics_override(override)
	end,
	on_join = function (player, data)
		local override = player:get_physics_override()
		override.jump = override.jump * 1.5
		player:set_physics_override(override)
	end,
	on_end = function (player, data)
		local override = player:get_physics_override()
		override.jump = override.jump / 1.5
		player:set_physics_override(override)
	end
})

effects.register("heavy", {
	on_apply = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity * 2
		player:set_physics_override(override)
	end,
	on_join = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity * 2
		player:set_physics_override(override)
	end,
	on_end = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity / 2
		player:set_physics_override(override)
	end
})
effects.register("light", {
	on_apply = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity / 2
		player:set_physics_override(override)
	end,
	on_join = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity / 2
		player:set_physics_override(override)
	end,
	on_end = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity * 2
		player:set_physics_override(override)
	end
})
effects.register("float", {
	on_apply = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity / 5
		player:set_physics_override(override)
	end,
	on_join = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity / 5
		player:set_physics_override(override)
	end,
	on_end = function (player, data)
		local override = player:get_physics_override()
		override.gravity = override.gravity * 5
		player:set_physics_override(override)
	end
})

effects.register("health", {
	on_apply = function (player, data)
		player:set_hp(player:get_hp() + 3)
	end
})
effects.register("regeneration", {
	on_step = function (player, data, dtime)
		if not data then data = {} end
		
		data.step = (data.step or 0) + dtime
		if data.step > 1 then data.step = 0 else return data end
		
		player:set_hp(player:get_hp() + 1)
		return data
	end
})

effects.register("damage", {
	on_apply = function (player, data)
		player:set_hp(player:get_hp() - 3)
	end
})
effects.register("poison", {
	on_step = function (player, data, dtime)
		if player:get_hp() <= 1 then return end
		
		if not data then data = {} end
		
		data.step = (data.step or 0) + dtime
		if data.step > 1 then data.step = 0 else return data end
		
		player:set_hp(player:get_hp() - 1)
		return data
	end
})
