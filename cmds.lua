minetest.register_chatcommand("effect", {
	params = "<subcommand> <target> <effect> (duration)",
	description = "Modify status effects. Subcommand can either be \"apply\" or \"remove\". Duration is only used when applying an effect.",
	privs = {effect = true},
	func = function (name, msg)
		local words = {}
		for w in msg:gmatch("[^%s]+") do
			table.insert(words, w)
		end
		
		local apply_words = {
			apply = true,
			app = true,
			add = true,
		}
		local remove_words = {
			remove = true,
			rem = true,
			delete = true,
			del = true,
		}
		
		local sub = words[1]
		
		local target_name = words[2]
		local effect = words[3]
		local duration = tonumber(words[4] or "")
		
		if apply_words[sub] then
			if not target_name then
				minetest.chat_send_player(name, "Invalid target name.")
				return
			elseif not effect then
				minetest.chat_send_player(name, "Invalid effect name.")
				return
			elseif not effects.registered[string.lower(effect)] then
				minetest.chat_send_player(name, "No such effect exists (case insensitive).")
				return
			elseif not duration then
				minetest.chat_send_player(name, "Invalid duration (should be a number).")
				return
			end
		
			local target = minetest.get_player_by_name(target_name)
			if not target then
				minetest.chat_send_player(name, "Target does not exist or is offline.")
				return
			end
			
			effects.apply(target, effect, duration)
			minetest.chat_send_player(name, "Applied effect successfully.")
		elseif remove_words[sub] then
			if not target_name then
				minetest.chat_send_player(name, "Invalid target name.")
				return
			elseif not effect then
				minetest.chat_send_player(name, "Invalid effect name.")
				return
			elseif not effects.registered[string.lower(effect)] then
				minetest.chat_send_player(name, "No such effect exists (case insensitive).")
				return
			end
		
			local target = minetest.get_player_by_name(target_name)
			if not target then
				minetest.chat_send_player(name, "Target does not exist or is offline.")
				return
			end
			
			if not effects.remove(target, effect) then
				minetest.chat_send_player(name, "Target does not have that effect.")
				return
			end
			minetest.chat_send_player(name, "Removed effect successfully.")
		else
			minetest.chat_send_player(name, "Invalid subcommand specified.")
		end
	end
})
minetest.register_chatcommand("cleareffects", {
	params = "<target>",
	description = "Removes all of the target's status effects.",
	privs = {effect = true},
	func = function (name, msg)
		local _, _, target_name = msg:find("([^%s]+)")
		
		if not target_name then
			minetest.chat_send_player(name, "Invalid target name.")
			return
		end
		
		local target = minetest.get_player_by_name(target_name)
		if not target then
			minetest.chat_send_player(name, "Target does not exist or is offline.")
			return
		end
		
		local fx = effects.get(target)
		for name,data in pairs(fx) do
			effects.remove(target, name)
		end
		minetest.chat_send_player(name, "Removed all effects successfully.")
	end
})
minetest.register_privilege("effect", {
	description = "Can grant and remove status effects to others, provided they are online",
	give_to_singleplayer = false
})
