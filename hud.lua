local effect_guis = {}

minetest.register_on_joinplayer(function (player)
	effect_guis[player:get_player_name()] = player:hud_add({
		id = "effects:effects",
		name = "effects:effects",
		
		position = {x=0.025,y=0.975},
		
		hud_elem_type = "text",
		
		scale = {x=100,y=100},
		number = 0xFFFFFF,
		alignment = {x=1,y=-1},
		
		offset = {x=0,y=0},
		
		text = "Loading..."
	})
end)
minetest.register_on_leaveplayer(function (player)
	effect_guis[player:get_player_name()] = nil
end)
minetest.register_globalstep(function (dtime)
	for _,player in pairs(minetest:get_connected_players()) do
		local player_effects = effects.get(player)
		local list = "Status effects:\n"
		--[[for name,info in pairs(player_effects) do
			list = list .. name .. " (" .. math.ceil(info.duration - (minetest.get_server_uptime() - info.start)) .. " seconds remaining)\n"
		end]]
		
		local affected = false
		for _,name in ipairs(effects.registered_list) do
			if player_effects[name] then
				-- local color = effects.registered[name].color or "#ffffff"
				
				local info = player_effects[name]
				local secs = --[[math.ceil(]]info.duration - (minetest.get_server_uptime() - info.start)--)
				
				local mins = math.floor(secs / 60)
				secs = math.floor(secs % 60)
				
				list = list .. string.format("%s    %2d:%02d\n", name, mins, secs)
				affected = true
			end
		end
		
		if not affected then
			list = list .. "none\n"
		end
		
		if player:hud_get("effects:effects") then player:hud_change(effect_guis[player:get_player_name()], "text", list) end
	end
end)
