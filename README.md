Status Effects mod for Minetest
===============================

Adds status effects to your Minetest world or server. A status effect modifies the physics, health and/or other attributes of players inflicted with it, in case that wasn't obvious enough.

API
---

* 'effects.register(name, descriptor)'    Registers a new status effect. 'name' should be a string, and is case insensitive.
    * 'descriptor' contains callbacks which start the effect, and end it. 'data' is a per-effect-per-player variable, kept in memory between calls.
        * 'on_apply = function (player, data)'    Callback to be run when the effect begins.
        * 'on_join = function (player, data)'    Callback to be run when the player leaves and rejoins, usually to re-set the player's physics_override.
        * 'on_step = function (player, data, dtime)'    Callback to be run on every globalstep.
        * 'on_end = function (player, data)'    Callback to be run when the effect ends, usually reversing the effects of on_apply.
* 'effects.apply(player, name, duration)'    Applies the given effect to a target.
* 'effects.remove(player, name)'    Forcefully removes the given effect from the target, ending it early. Returns true if the target actually had the effect.
* 'effects.get(player)'    Gets the deserialized table of effects the given player has.
    * Each effect instantiation has the following properties:
        * 'start'    The time since the server began, when the effect was first inflicted. Negative numbers indicate that the effect was inflicted before the last server restart.
        * 'duration'    How long the effect lasts for.
        * 'memory'    The 'data' stored above.
        * 'last'    Auxillary variable set at each globalstep to the current server uptime.
* 'effects.set(player, t)'    Sets the table of effects the player has, to 't'.

Commands
--------

* '/effect [apply/remove] [target] [effect] (duration)'    Applies or removes the given effect from the target.
* '/cleareffects [target]'    Removes every effect from the target.
