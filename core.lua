-- We create a table of registered effects to store each status effect in.
-- effects.registered maps an effect name to its descriptor.
-- effects.registered_list maps an integer to an effect name, for use in the HUD to avoid glitchiness.
effects = {}
effects.registered = {}
effects.registered_list = {}
effects.register = function (name, descriptor)
	local name = string.lower(name)
	effects.registered[name] = descriptor
	table.insert(effects.registered_list, name)
end

-- "effects_core" contains various core functions which others invoke.
local effects_core = {}
effects_core.apply = function (player, name, ...)
	-- Run the on_apply function of the named effect on this player, provided such an effect and such a function exists.
	local effect = effects.registered[name]
	if effect and type(effect) == "table" and effect.on_apply then
		return effect.on_apply(player, ...)
	end
end
effects_core.reapply = function (player, name, ...)
	-- Run the on_join function of the named effect on this player, provided such an effect and such a function exists.
	local effect = effects.registered[name]
	if effect and type(effect) == "table" and effect.on_join then
		return effect.on_join(player, ...)
	end
end
effects_core.cancel = function (player, name, ...)
	-- Run the on_end function of the named effect on this player, provided such an effect and such a function exists.
	local effect = effects.registered[name]
	if effect and type(effect) == "table" and effect.on_end then
		return effect.on_end(player, ...)
	end
end
effects_core.get = function (player)
	-- Get the table of effects that this player has.
	return minetest.deserialize(player:get_attribute("effects:list") or "") or {}
end
effects_core.set = function (player, t)
	-- Modify the table of effects that this player has.
	player:set_attribute("effects:list", minetest.serialize(t or {}) or "")
end

-- These are functions other mods can use to apply and remove status effects when desired.
effects.apply = function (player, name, duration)
	-- Apply an effect to the player, setting all of the necessary data in the effects:list.
	local player_effects = effects_core.get(player)
	local already_applied = false
	if not player_effects[name] then
		player_effects[name] = {}
	else
		already_applied = true
	end
	
	player_effects[name].start = player_effects[name].start or minetest.get_server_uptime()
	player_effects[name].duration = (player_effects[name].duration or 0) + duration
	player_effects[name].memory = player_effects[name].memory or {}
	
	effects_core.set(player, player_effects)
	if not already_applied then
		player_effects[name].memory = effects_core.apply(player, name, player_effects[name].memory)
	end
	return true
end
effects.remove = function (player, name, duration)
	-- Remove an effect from the player, setting all of the necessary data in the effects:list.
	local player_effects = effects_core.get(player)
	if not player_effects[name] then
		return false
	end
	
	effects_core.cancel(player, name, player_effects[name].memory)
	
	player_effects[name] = nil
	effects_core.set(player, player_effects)
	return true
end

-- When a player joins, see what status effects they have and call their on_join callbacks.
-- Additionally, we use the "left" value to recalculate the correct amount of time remaining when a player leaves and rejoins.
minetest.register_on_joinplayer(function (player)
	local player_effects = effects_core.get(player)
	for name,info in pairs(player_effects) do
		if info.left then
			player_effects[name].start = minetest.get_server_uptime() - (info.left - info.start)
			player_effects[name].left = nil
			info = player_effects[name]
		end
		
		local total_duration = info.duration
		local duration_elapsed = minetest.get_server_uptime() - info.start
		
		if duration_elapsed < total_duration then		
			player_effects[name].memory = effects_core.reapply(player, name, info.memory)
		else
			player_effects[name] = nil
		end
	end
	effects_core.set(player, player_effects)
end)
minetest.register_on_dieplayer(function (player)
	-- The player died - effects shouldn't carry through respawns. Remove each effect.
	for name,data in pairs(effects_core.get(player)) do
		effects.remove(player, name)
	end
end)
minetest.register_globalstep(function (dtime)
	-- For each player...
	for _,player in pairs(minetest.get_connected_players()) do
		-- Get the list of effects.
		local player_effects = effects_core.get(player)
		-- For each effect instance...
		for name,info in pairs(player_effects) do
			-- Get the descriptor of the effect.
			local descriptor = effects.registered[name]
			player_effects[name].left = minetest.get_server_uptime()
			-- If the descriptor has an on_step function, run it.
			if descriptor and descriptor.on_step then
				player_effects[name].memory = descriptor.on_step(player, info.memory, dtime)
			end
			-- Remove the effect when it runs out.
			if info.start + info.duration < minetest.get_server_uptime() then
				effects_core.cancel(player, name)
				player_effects[name] = nil
			end
			effects_core.set(player, player_effects)
		end
	end
end)

effects.get = effects_core.get
effects.set = effects_core.set
